package com.devcamp.task54b_20;

public class Cylinder extends Circle{

    public double getHeight() {
        return this.height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "{" +
            " height='" + getHeight() + "'" +
            "}";
    }
    public double getVolume() {
        return this.getArea()*this.height;
    }
    public Cylinder(double height) {
        this.height = height;
    }
    public Cylinder(double radius, double height) {
        this.height = height;
        this.setRadius(radius);
    }
    public Cylinder(double radius, double height, String color) {
        this.height = height;
        this.setRadius(radius);
        this.setColor(color);
    }

    public Cylinder() {
    }
    private double height;
    
}
