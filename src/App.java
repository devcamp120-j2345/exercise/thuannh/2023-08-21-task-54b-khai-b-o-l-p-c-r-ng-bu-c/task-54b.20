import com.devcamp.task54b_20.Circle;
import com.devcamp.task54b_20.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0,"green");
        System.out.println("Circle1: " + circle1 +" has Area: " + circle1.getArea());
        System.out.println("Circle2: " + circle2 +" has Area: " + circle2.getArea());
        System.out.println("Circle3: " + circle3 +" has Area: " + circle3.getArea());

        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.5);
        Cylinder cylinder3 = new Cylinder(3.5,1.5);
        Cylinder cylinder4 = new Cylinder(3.5,1.5, "green");
        System.out.println("Cylinder1 has: " + cylinder1.getVolume());
        System.out.println("Cylinder2 has: " + cylinder2.getVolume());
        System.out.println("Cylinder3 has: " + cylinder3.getVolume());
        System.out.println("Cylinder4 has: " + cylinder4.getVolume());
    }
}
